const search = document.querySelector('.search');
const content = document.querySelector('.content');


const fetchIP = async () => {
    try {
        const searchIp = await fetch('https://api.ipify.org/?format=json').then(data => data.json())
        const searchInfo = await fetch(`http://ip-api.com/json/${searchIp.ip}?fields=status,continent,country,regionName,city,district,query`).then(data => data.json())
        if (searchInfo.query === searchIp.ip) {
            if (!document.querySelector('.address')) {
                content.insertAdjacentHTML('beforeend', `
                <ul class="address">
                    <li class="address__item">
                        <strong>континент:</strong> ${searchInfo.continent}
                    </li>
                    <li class="address__item">
                        <strong>країна:</strong> ${searchInfo.country}
                    </li>
                    <li class="address__item">
                        <strong>регіон:</strong> ${searchInfo.regionName}
                    </li>
                    <li class="address__item">
                        <strong>місто:</strong> ${searchInfo.city}
                    </li>
                    <li class="address__item">
                        <strong>район:</strong> ${searchInfo.district || 'Unknown'}
                    </li>
                </ul>
            `)
            }
        }
    } catch (err) {
        console.error(err);
    }
}


search.addEventListener('click', fetchIP);